var express     	=   require('express');
var bodyParser  	=   require('body-parser');
var bodyCleaner 	=   require('express-body-cleaner');
var request         =   require('request');
var passport        =   require('passport');
var uberStrategy    =   require('passport-uber-v2').Strategy;
var config      	=   require('./config');
var mongoose    	=   require('mongoose');
var driver        	=   require('./api/models/uber-driver-details');
var app         	=   express();
var port        	=   process.env.PORT || 8080;
var redirect        =   "";

mongoose.connect(config.database, {useNewUrlParser:true});

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(bodyCleaner);
app.use(express.static(__dirname + '/public'));
app.use(require('express-session')({secret: 'keyboard cat',resave: true,saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(new uberStrategy({
    clientID: "Cy7ICaiaXJIziT8nse34-PlZeu5-IAq6",
    clientSecret: "WU8nMF0l2E92yiHArK4UJLvoIMreAQDTr-5YEWbz",
    callbackURL: 'https://uberlogin.herokuapp.com/callback'
},
function(accessToken, refreshToken, profile, done) {
        var user = profile;
        user.accessToken = accessToken;
        return done(null, user);
    }
));

app.get('/',function(req,res){
    if (req.user) {
        res.redirect('/me');
    } else {
        res.render('index.ejs');
    }
});

app.get('/login/uber',
    passport.authenticate('uber',{scope: ['profile']}
));

app.get('/callback', passport.authenticate('uber',{failureRedirect: '/'}),function(req, res) {
    if (redirect) {
        res.redirect('/redirect?url='+redirect);
    }
    else {
        res.redirect('/me');
    }
});

app.get('/redirect',function(req,res) {
    if (req.query.url == "" || req.query.url == null) {
        res.send();
    }
    else {
        if (req.user) {
            request.post(
                req.query.url,{ 
                    headers: {
                        "Content-Type": "application/json"
                    },
                    form: {
                        "uuid": req.user.id,
                        "firstname": req.user.firstname,
                        "lastname": req.user.lastname,
                        "email": req.user.email,
                        "picture": req.user._json.picture,
                        "promo_code": req.user._json.promo_code,
                        "rider_id": req.user._json.rider_id,
                        "mobile_verified": req.user._json.mobile_verified
                    }
                },
                function (error, response, body) {
                    console.log(response);
                    if (response.statusCode == 200) {
                        res.send("Uber data POSTed successfully!");
                    }
                    else {
                        res.status(401).send("Something went wrong!");
                    }
                }
            );
        }
        else {
            redirect = req.query.url;
            res.redirect('/login/uber');
        }
    }
});

app.post('/savedriver', function(req,res){
    var newdriver = driver({ 
        uberuuid: req.body.uuid,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        avatar: req.body.picture,
        promo_code: req.body.promo_code,
        mobile_verified: req.body.mobile_verified
    });
    newdriver.save(function(err) {
        if (err) {
            res.status(417).send({
                success: false,
                message: err
            });
        }
        else {
            res.send("Uber data saved successfully!");
        }
    });
});

app.get('/me',function(req,res) {
    if (req.user) {
        res.render('profile.ejs', {
            user : req.user
        });
    } else {
        res.redirect('/');
    }
});

app.get('/me/raw',function(req,res) {
    if (req.user) {
        res.send(req.user);
    } else {
        res.redirect('/');
    }
});

app.get('/logout',function(req,res){
    redirect = null;
    req.logout();
    res.redirect('/');
});

app.listen(port);
console.log('Magic happens at http://localhost:' + port);