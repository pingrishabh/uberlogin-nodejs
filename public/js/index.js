$("#star").hover(
  function() {
    $(".hover-box").css("z-index", 1);
    $(".hover-box").css("opacity", 1);
  },
  function() {
    $(".hover-box").css("z-index", -1);
    $(".hover-box").css("opacity", 0);
  }
);