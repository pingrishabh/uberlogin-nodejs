var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('uber-driver-details', new Schema({
    uberuuid: {
        type: String,
        default: null
    },
    firstname: {
        type: String,
        default: null
    },
    lastname: {
        type: String,
        default: null
    },
    email: {
        type: String,
        default: null
    },
    avatar: {
        type: String,
        default: null
    },
    promo_code: {
        type: String,
        default: null
    },
    mobile_verified: {
        type: Boolean,
        default: null
    }
}));